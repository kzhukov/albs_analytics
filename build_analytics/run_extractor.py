"""
extractor startup script
"""
import sys
from build_analytics.extractor.start import start

try:
    YAML_PATH = sys.argv[1]
except IndexError:
    print(f"Usage: {sys.argv[0]} config.yml")
    sys.exit(1)
start(YAML_PATH)
