BEGIN;

INSERT INTO arch_enum (id, value)
VALUES
    (5, 'src'),
    (6, 'x86_64_v2');

UPDATE schema_version
SET version = 4;

COMMIT;