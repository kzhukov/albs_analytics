BEGIN;

-- builds
CREATE TABLE builds (
    id INTEGER PRIMARY KEY,
    url VARCHAR(50) NOT NULL,
    created_at DOUBLE PRECISION NOT NULL,
    finished_at DOUBLE PRECISION
);


CREATE INDEX IF NOT EXISTS builds_created_at
ON builds(created_at);

CREATE INDEX IF NOT EXISTS builds_finished_at
ON builds(finished_at);


-- build_tasks_enum
CREATE TABLE IF NOT EXISTS build_task_status_enum(
    id INTEGER PRIMARY KEY,
    value VARCHAR(15)
);

INSERT INTO build_task_status_enum (id, value)
VALUES
    (0, 'idle'),
    (1, 'started'),
    (2, 'completed'),
    (3, 'failed'),
    (4, 'excluded');


-- arch_enum
CREATE TABLE arch_enum(
    id INTEGER PRIMARY KEY,
    value VARCHAR(15)
);

INSERT INTO arch_enum(id, value)
VALUES
    (0, 'i686'),
    (1, 'x86_64'),
    (2, 'aarch64'),
    (3, 'ppc64le'),
    (4, 's390x');


-- web_node_stats_enum
CREATE TABLE web_node_stats_enum (
     id INTEGER PRIMARY KEY,
    value VARCHAR(50)
);

INSERT INTO web_node_stats_enum (id, value)
VALUES
    (0, 'build_done'),
    (1, 'logs_processing'),
    (2, 'packages_processing');


-- build_node_stats_enum
CREATE TABLE build_node_stats_enum(
    id INTEGER PRIMARY KEY,
    value VARCHAR(50)
);

INSERT INTO build_node_stats_enum (id, value)
VALUES
    (0, 'upload'),
    (1, 'build_all'),
    (2, 'build_srpm'),
    (3, 'git_checkout'),
    (4, 'build_binaries'),
    (5, 'build_packages'),
    (6, 'build_node_task'),
    (7, 'cas_notarize_artifacts'),
    (8, 'cas_source_authenticate');


-- build_tasks
CREATE TABLE build_tasks (
    id INTEGER PRIMARY KEY,
    name VARCHAR(50) NOT NULL,
    build_id INTEGER REFERENCES builds(id) ON DELETE CASCADE,
    arch_id INTEGER REFERENCES arch_enum(id) ON DELETE SET NULL,
    status_id INTEGER REFERENCES build_task_status_enum(id) ON DELETE SET NULL,
    started_at DOUBLE PRECISION,
    finished_at DOUBLE PRECISION
);

CREATE INDEX build_tasks_build_id
ON build_tasks(build_id);

CREATE INDEX build_tasks_started_at
ON build_tasks(started_at);

CREATE INDEX build_tasks_finished_at
ON build_tasks(finished_at);


-- web_node_stats
CREATE TABLE web_node_stats (
    build_task_id INTEGER REFERENCES build_tasks(id) ON DELETE CASCADE,
    stat_name_id  INTEGER REFERENCES web_node_stats_enum(id) ON DELETE SET NULL,
    start_ts DOUBLE PRECISION,
    end_ts DOUBLE PRECISION
);

CREATE INDEX web_node_stats_build_task_id
ON web_node_stats(build_task_id);

CREATE INDEX web_node_stats_start_ts
ON web_node_stats(start_ts);

CREATE INDEX web_node_stats_end_ts
ON web_node_stats(end_ts);


-- build_node_stats
CREATE TABLE build_node_stats (
    build_task_id INTEGER REFERENCES build_tasks(id) ON DELETE CASCADE,
    stat_name_id  INTEGER REFERENCES build_node_stats_enum(id) ON DELETE SET NULL,
    start_ts DOUBLE PRECISION,
    end_ts DOUBLE PRECISION
);

CREATE INDEX build_node_stats_build_task_id
ON build_node_stats(build_task_id);

CREATE INDEX build_node_stats_build_start_ts
ON build_node_stats(start_ts);

CREATE INDEX build_node_stats_build_end_ts
ON build_node_stats(end_ts);


-- sign_tasks
CREATE TABLE sign_tasks (
    id INTEGER PRIMARY KEY,
    build_id INTEGER REFERENCES builds(id) ON DELETE CASCADE,
    buildtask_id INTEGER REFERENCES build_tasks(id) ON DELETE CASCADE,
    started_at DOUBLE PRECISION,
    finished_at DOUBLE PRECISION
);

CREATE INDEX sign_tasks_build_id
ON sign_tasks(build_id);

CREATE INDEX sign_tasks_buildtask_id
ON sign_tasks(buildtask_id);

CREATE INDEX sing_tasks_started_at
ON sign_tasks(started_at);

CREATE INDEX sign_tasks_finished_at
ON sign_tasks(finished_at);


-- schema_version
CREATE TABLE schema_version (
    version INTEGER
);
INSERT INTO  schema_version (version)
VALUES (1);

COMMIT;