BEGIN;

INSERT INTO web_node_stats_enum (id, value)
VALUES
    (3, 'multilib_processing');

ALTER TABLE web_node_stats
ADD CONSTRAINT web_node_stats_unique UNIQUE (build_task_id, stat_name_id);

ALTER TABLE build_node_stats
ADD CONSTRAINT build_node_stats_unique UNIQUE (build_task_id, stat_name_id);


INSERT INTO build_task_status_enum (id, value)
VALUES
    (5, 'canceled');

UPDATE schema_version
SET version = 2;

COMMIT;