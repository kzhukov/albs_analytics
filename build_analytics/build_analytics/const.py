# pylint: disable=invalid-name

from enum import IntEnum

# supported schema version
DB_SCHEMA_VER = 4


# ENUMS
class ArchEnum(IntEnum):
    i686 = 0
    x86_64 = 1
    aarch64 = 2
    ppc64le = 3
    s390x = 4
    src = 5
    x86_64_v2 = 6


class BuildTaskEnum(IntEnum):
    idle = 0
    started = 1
    completed = 2
    failed = 3
    excluded = 4
    canceled = 5


class WebNodeStatsEnum(IntEnum):
    build_done = 0
    logs_processing = 1
    packages_processing = 2
    multilib_processing = 3


class BuildNodeStatsEnum(IntEnum):
    upload = 0
    build_all = 1
    build_srpm = 2
    git_checkout = 3
    build_binaries = 4
    build_packages = 5
    build_node_task = 6
    cas_notarize_artifacts = 7
    cas_source_authenticate = 8


class TestTaskStatusEnum(IntEnum):
    created = 1
    started = 2
    completed = 3
    failed = 4


class TestStepEnum(IntEnum):
    install_package = 0
    stop_enviroment = 1
    initial_provision = 2
    start_environment = 3
    uninstall_package = 4
    initialize_terraform = 5
    package_integrity_tests = 6
    stop_environment = 7
