from datetime import datetime
from typing import Optional

from pydantic import BaseModel  # pylint: disable=no-name-in-module

from .test_task_db import TestTaskDB
from .test_steps_stats import TestStepsStats


class TestTask(BaseModel):
    id: int
    build_task_id: int
    revision: int
    status: int
    package_fullname: str
    started_at: Optional[datetime] = None
    steps_stats: Optional[TestStepsStats] = None

    def as_db_model(self) -> TestTaskDB:
        started_at = self.started_at.timestamp() \
            if self.started_at else None
        params = {
            'id': self.id,
            'build_task_id': self.build_task_id,
            'revision': self.revision,
            'status_id': self.status,
            'package_fullname': self.package_fullname,
            'started_at': started_at,
            'steps_stats': self.steps_stats.as_db(self.id) if self.steps_stats else None
        }
        return TestTaskDB(**params)
