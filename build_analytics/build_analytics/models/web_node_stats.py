from typing import List

from pydantic import BaseModel  # pylint: disable=no-name-in-module


from .build_stat import BuildStat
from .web_node_stat_db import WebNodeStatDB
from ..const import WebNodeStatsEnum


class WebNodeStats(BaseModel):
    """
    Represents build statistics for web node
    """
    build_done: BuildStat
    logs_processing: BuildStat
    packages_processing: BuildStat
    multilib_processing: BuildStat

    def as_db_model(self, build_task_id: int) -> List[WebNodeStatDB]:
        result = []
        for field_name in self.__fields__.keys():

            stats: BuildStat = getattr(self, field_name)
            start_ts = stats.start_ts.timestamp() \
                if stats.start_ts else None
            end_ts = stats.end_ts.timestamp() \
                if stats.end_ts else None
            stat_name_id = WebNodeStatsEnum[field_name].value

            web_node_stat_db = WebNodeStatDB(build_task_id=build_task_id,
                                             stat_name_id=stat_name_id,
                                             start_ts=start_ts,
                                             end_ts=end_ts)
            result.append(web_node_stat_db)
        return result
