from typing import List, Optional

from pydantic import BaseModel  # pylint: disable=no-name-in-module

from ..const import TestStepEnum
from .test_step_stat import TestStepStat
from .test_step_stat_db import TestStepStatDB


class TestStepsStats(BaseModel):
    install_package: Optional[TestStepStat] = None
    stop_environment: Optional[TestStepStat] = None
    initial_provision: Optional[TestStepStat] = None
    start_environment: Optional[TestStepStat] = None
    uninstall_package: Optional[TestStepStat] = None
    initialize_terraform: Optional[TestStepStat] = None
    package_integrity_tests: Optional[TestStepStat] = None

    def as_db(self, test_task_id: int) -> List[TestStepStatDB]:
        result = []
        for field_name in self.__fields__.keys():
            stats: TestStepStat = getattr(self, field_name)
            if not stats:
                continue
            start_ts = stats.start_ts.timestamp() \
                if stats.start_ts else None
            finish_ts = stats.finish_ts.timestamp() \
                if stats.finish_ts else None
            stat_name_id = TestStepEnum[field_name].value

            test_step_stat_db = TestStepStatDB(test_task_id=test_task_id,
                                               stat_name_id=stat_name_id,
                                               start_ts=start_ts,
                                               finish_ts=finish_ts)
            result.append(test_step_stat_db)
        return result
