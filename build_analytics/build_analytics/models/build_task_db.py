from typing import Optional
from pydantic import BaseModel  # pylint: disable=no-name-in-module


class BuildTaskDB(BaseModel):
    """
    BuildTask as it received from/sent to database
    """
    id: int
    name: str
    build_id: int
    arch_id: int
    started_at: Optional[float] = None
    finished_at: Optional[float] = None
    status_id: int
