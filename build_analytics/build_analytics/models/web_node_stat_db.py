
from pydantic import BaseModel  # pylint: disable=no-name-in-module
from typing import Optional


class WebNodeStatDB(BaseModel):
    """
    Represents WebNodeStat as it sent to/received from databse
    """
    build_task_id: int
    stat_name_id: int
    start_ts: Optional[float] = None
    end_ts: Optional[float] = None
