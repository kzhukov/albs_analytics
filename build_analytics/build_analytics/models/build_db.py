from typing import Optional
from pydantic import BaseModel, HttpUrl  # pylint: disable=no-name-in-module


class BuildDB(BaseModel):
    """
    Build as it received from/sent to database
    """
    id: int
    url: HttpUrl
    created_at: int
    finished_at: Optional[float] = None
