from datetime import datetime
from typing import List, Optional
from pydantic import BaseModel, HttpUrl  # pylint: disable=no-name-in-module

from .build_task import BuildTask
from .build_db import BuildDB


class Build(BaseModel):
    id: int
    url: HttpUrl
    build_tasks: List[BuildTask]
    created_at: datetime
    finished_at: Optional[datetime] = None

    def as_db_model(self) -> BuildDB:
        created_at = self.created_at.timestamp()
        finished_at = self.finished_at.timestamp() \
            if self.finished_at else None
        params = {
            'id': self.id,
            'url': self.url,
            'created_at': created_at,
            'finished_at': finished_at
        }
        return BuildDB(**params)
