"""
Module for BuildStat model
"""

from datetime import datetime
from typing import Optional
from pydantic import BaseModel  # pylint: disable=no-name-in-module


class BuildStat(BaseModel):
    """
    BuildStat represents particular build statistic
    """
    start_ts: Optional[datetime] = None
    end_ts: Optional[datetime] = None
