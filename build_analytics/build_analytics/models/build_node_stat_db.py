from pydantic import BaseModel  # pylint: disable=no-name-in-module
from typing import Optional


class BuildNodeStatDB(BaseModel):
    """
    Build node stat as it sent to/received from database
    """
    build_task_id: int
    stat_name_id: int
    start_ts: Optional[float] = None
    end_ts: Optional[float] = None
