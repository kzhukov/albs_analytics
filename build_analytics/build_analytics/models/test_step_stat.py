from datetime import datetime
from typing import Optional

from pydantic import BaseModel  # pylint: disable=no-name-in-module


class TestStepStat(BaseModel):
    start_ts: Optional[datetime] = None
    finish_ts: Optional[datetime] = None
