from pydantic import BaseModel  # pylint: disable=no-name-in-module
from typing import Optional


class TestStepStatDB(BaseModel):
    test_task_id: int
    stat_name_id: int
    start_ts: Optional[float] = None
    finish_ts: Optional[float] = None
