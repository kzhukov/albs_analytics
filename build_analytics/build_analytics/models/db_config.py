from pydantic import BaseModel, Field


DB_PORT = 5432
DB_HOST = "localhost"


class DbConfig(BaseModel):
    name: str = Field(description="db name")
    port: int = Field(description="db server port", default=DB_PORT)
    host: str = Field(description="db server ip/hostname", default=DB_HOST)
    username: str = Field(description="username to connect with")
    password: str = Field(description="password to connect with1")
