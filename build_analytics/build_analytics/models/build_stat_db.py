"""
Module for BuildStatDB model
"""


from pydantic import BaseModel  # pylint: disable=no-name-in-module


class BuildStatDB(BaseModel):
    """
    Represents build stat as it send to/received from database
    """
    build_task_id: int
    stat_name_id: int
    start_ts: float
    end_ts: float
