from datetime import datetime
from typing import Optional

from pydantic import BaseModel  # pylint: disable=no-name-in-module

from .build_task_db import BuildTaskDB
from .build_node_stats import BuildNodeStats
from ..const import ArchEnum
from .web_node_stats import WebNodeStats


class BuildTask(BaseModel):
    id: int
    name: str
    build_id: int
    arch: str
    started_at: Optional[datetime] = None
    finished_at: Optional[datetime] = None
    status_id: int
    build_node_stats: BuildNodeStats
    web_node_stats: WebNodeStats

    def as_db_model(self) -> BuildTaskDB:
        started_at = self.started_at.timestamp() \
            if self.started_at else None
        finished_at = self.finished_at.timestamp() \
            if self.finished_at else None
        params = {
            'id': self.id,
            'name': self.name,
            'build_id': self.build_id,
            'arch_id': ArchEnum[self.arch].value,
            'started_at': started_at,
            'finished_at': finished_at,
            'status_id': self.status_id
        }
        return BuildTaskDB(**params)
