from typing import List

from pydantic import BaseModel  # pylint: disable=no-name-in-module


from .build_stat import BuildStat
from .build_node_stat_db import BuildNodeStatDB
from ..const import BuildNodeStatsEnum


class BuildNodeStats(BaseModel):
    """
    Represents build statistics for build node
    """
    build_all: BuildStat
    build_binaries: BuildStat
    build_packages: BuildStat
    build_srpm: BuildStat
    build_node_task: BuildStat
    cas_notarize_artifacts: BuildStat
    cas_source_authenticate: BuildStat
    git_checkout: BuildStat
    upload:  BuildStat

    def as_db_model(self, build_task_id: int) -> List[BuildNodeStatDB]:
        result = []
        for field_name in self.__fields__.keys():

            stats: BuildStat = getattr(self, field_name)
            start_ts = stats.start_ts.timestamp() \
                if stats.start_ts else None
            end_ts = stats.end_ts.timestamp() \
                if stats.end_ts else None
            stat_name_id = BuildNodeStatsEnum[field_name].value

            build_node_stat_db = BuildNodeStatDB(build_task_id=build_task_id,
                                                 stat_name_id=stat_name_id,
                                                 start_ts=start_ts,
                                                 end_ts=end_ts)
            result.append(build_node_stat_db)
        return result
