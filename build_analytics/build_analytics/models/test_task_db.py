from typing import List, Optional
from pydantic import BaseModel  # pylint: disable=no-name-in-module

from .test_step_stat_db import TestStepStatDB


class TestTaskDB(BaseModel):
    """
    Test task as it received from/sent to database
    """
    id: int
    build_task_id: int
    revision: int
    status_id: int
    package_fullname: str
    started_at: Optional[float] = None
    steps_stats: Optional[List[TestStepStatDB]] = None
