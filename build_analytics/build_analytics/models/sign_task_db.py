from pydantic import BaseModel  # pylint: disable=no-name-in-module


class SignTaskDB(BaseModel):
    """
    SignTaskDB as it received from/sent to database
    """
    id: int
    build_id: int
    build_task_id: int
    started_at: int
    finished_at: int
